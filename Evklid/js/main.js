const burger_open = document.querySelector('.header_burger')
const burger_close = document.querySelector('.header_burger_close')
const menu = document.querySelector('.mobile_menu')

burger_open.addEventListener('click', showMenu)
burger_close.addEventListener('click', hideMenu)

function showMenu() {
    menu.classList.add('mobile_menu_show')
}
function hideMenu() {
    menu.classList.remove('mobile_menu_show')
}
